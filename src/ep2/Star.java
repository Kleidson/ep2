package ep2;

public class Star extends Cobra {
	public Star(Cobra cobra) {
		setIDCobra(3);
		setTamanho(cobra.getTamanho());
		setX(cobra.getX());
		setY(cobra.getY()); 
		setCima(cobra.isCima());
		setBaixo(cobra.isBaixo());
		setEsquerda(cobra.isEsquerda());
		setDireita(cobra.isDireita());
		setPontuacao(cobra.getPontuacao());	
	}
}
