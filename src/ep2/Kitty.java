package ep2;

public class Kitty extends Cobra {
	public Kitty(Cobra cobra) {
		setIDCobra(2);
		setTamanho(cobra.getTamanho());
		setX(cobra.getX());
		setY(cobra.getY()); 
		setCima(cobra.isCima());
		setBaixo(cobra.isBaixo());
		setEsquerda(cobra.isEsquerda());
		setDireita(cobra.isDireita());
		setPontuacao(cobra.getPontuacao());	
	}
	}
