package ep2;

public class Barreira {
	private int [] x = new int [70];
	private int [] y = new int [70]; 
	private int quantidade; 
	public Barreira () {
		quantidade =10;
		for(int i=0; i<quantidade ; i++) {
			x[i]= 50 - i*10;
			y[i] = 50;
		}
		GerarBarreira(); 
	}
	public void GerarBarreira() {
		for(int i=0; i<quantidade;i++) {
				x[i]= (int)(Math.random() * 35)*10 +60;
				y[i] = (int)(Math.random() * 28)*10;
				for(int j=0; j<quantidade;j++) {
					if(x[i]== x[j] && y[i] == y[j]) {
						x[i]= (int)(Math.random() * 35)*10 +60;
						y[i] = (int)(Math.random() * 28)*10;
					} 
				}
			}
			
		for(int j=quantidade; j<(7*quantidade); j++) {
			int aleatorio = (int)(Math.random() * 2)+1; 
			if(aleatorio == 1) {
				x[j] = x[j-quantidade]+10; 
				y[j]= y[j-quantidade]; 
			}
			else {
				x[j] = x[j-quantidade]; 
				y[j]= y[j-quantidade]+10; 
			}
		}
	}
	public int getX(int i) {
		return x[i];
	}
	public void setX(int x, int pos) {
		this.x[pos] = x;
	}
	public int getY(int i) {
		return y[i];
	}
	public void setY(int y, int pos) {
		this.y[pos] = y;
	}
	public int getQuantidade() {
		return quantidade; 
	}
}
