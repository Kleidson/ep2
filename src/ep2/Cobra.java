package ep2;

public class Cobra {
	private int IDCobra; 
	private int tamanho; 
	private int [] x = new int [500]; 
	private int [] y = new int [500];
	private int pontuacao = 0; 
	private boolean direita = false;
	private boolean esquerda = false;
	private boolean cima = false;
	private boolean baixo = false;
	private boolean viva = true; 
	final static int KITTY = 2; 
	public Cobra() {
		IDCobra = 1;
		tamanho = 3; 
		for(int i=0; i<tamanho; i++) {
			x[i]= 50 - i*10;
			y[i] = 50; 	
		}	
	}
	public Cobra(Cobra cobra) {
		setIDCobra(1);
		setTamanho(cobra.getTamanho());
		setX(cobra.getX());
		setY(cobra.getY()); 
		setCima(cobra.isCima());
		setBaixo(cobra.isBaixo());
		setEsquerda(cobra.isEsquerda());
		setDireita(cobra.isDireita());
		setPontuacao(cobra.getPontuacao());	
		setViva(cobra.isViva()); 
	}
	
	public void mover() {
			if(direita || baixo || cima || esquerda) {
				for(int i=tamanho; i>0 ; i--) {
					x[i] = x[(i-1)];
					y[i] = y[(i-1)]; 
					}
			}
		if(direita) {
			x[0]+=10;
			if(x[0]>480 && IDCobra != KITTY) {
				x[0] = 0;
			}
		}
		else if(esquerda) {
			x[0]-=10;
			if(x[0]<0 && IDCobra != KITTY) {
				x[0] = 480;
			}
		}
		else if(cima) {
			y[0]-=10;
			if(y[0]<0 && IDCobra != KITTY) {
				y[0] = 400;
			}
		}
		else if(baixo) {
			y[0]+=10; 
			if(y[0]>400 && IDCobra != KITTY) {
				y[0] = 0;
			}
		}
	}

	public int getTamanho() {
		return tamanho;
	}

	public void setTamanho(int tamanho) {
		this.tamanho = tamanho;
	}

	public int getX(int i) {
		return x[i];
	}

	public void setX(int x, int i) {
		this.x[i] = x;
	}

	public int getY(int i) {
		return y[i];
	}

	public void setY(int y,int i) {
		this.y[i] = y;
	}

	public int[] getX() {
		return x;
	}

	public void setX(int[] x) {
		this.x = x;
	}

	public int[] getY() {
		return y;
	}

	public void setY(int[] y) {
		this.y = y;
	}
	public boolean isDireita() {
		return direita;
	}

	public void setDireita(boolean direita) {
		this.direita = direita;
	}

	public boolean isEsquerda() {
		return esquerda;
	}

	public void setEsquerda(boolean esquerda) {
		this.esquerda = esquerda;
	}

	public boolean isCima() {
		return cima;
	}

	public void setCima(boolean cima) {
		this.cima = cima;
	}

	public boolean isBaixo() {
		return baixo;
	}

	public void setBaixo(boolean baixo) {
		this.baixo = baixo;
	}

	public boolean isViva() {
		return viva;
	}

	public void setViva(boolean viva) {
		this.viva = viva;
	}

	public int getPontuacao() {
		return pontuacao;
	}

	public void setPontuacao(int pontuacao) {
		this.pontuacao = pontuacao;
	}

	public int getIDCobra() {
		return IDCobra;
	}

	public void setIDCobra(int iDCobra) {
		IDCobra = iDCobra;
	}

	
	
	
	
}
