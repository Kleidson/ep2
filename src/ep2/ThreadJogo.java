package ep2;

public class ThreadJogo extends Thread {
	private PainelJogo painel; 
	private Interations intera; 
	ThreadJogo(PainelJogo painel){
		this.painel = painel; 
		 intera = new Interations(painel,painel.getCobra(),painel.getFruta(),painel.getFruta2(), painel.getBarreiras());
	}
	public void run () {
		while(painel.getCobra().isViva()) {
		try {
			Thread.sleep(110);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		intera = new Interations(painel, painel.getCobra(),painel.getFruta(),painel.getFruta2(), painel.getBarreiras());
		if(!painel.isPause()) {
		intera.Interacoes(painel.getCobra().getIDCobra(),painel.getFruta().getIDFruta(), painel.getFruta2().getIDFruta());
		painel.setFruta(intera.getFruta());
		painel.setFruta2(intera.getFruta2());
		painel.getCobra().mover();
		}
		if(!painel.getCobra().isViva()) {
			painel.setCobra(new Cobra(painel.getCobra()));
			painel.setKitty(false);
			painel.setStar(false);
		}
		
		painel.repaint();
	}
		
	}
}
