package ep2;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;

public class Jogo extends JFrame {
	private PainelJogo painel = new PainelJogo();
	private ThreadJogo thread = new ThreadJogo(painel);
	
	public static void main(String[] args) {
		new Jogo(); 
	}
	
	public Jogo() {
		add(painel);
		thread.start();
		addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				
			}

			@Override
			public void keyPressed(KeyEvent e) {
				int comando = e.getKeyCode(); 
				if(comando == KeyEvent.VK_LEFT && !painel.getCobra().isDireita() && !painel.isPause()) {
					painel.getCobra().setEsquerda(true);
					painel.getCobra().setCima(false);
					painel.getCobra().setBaixo(false); 
				}
				else if(comando == KeyEvent.VK_RIGHT && !painel.getCobra().isEsquerda() && !painel.isPause()) {
					painel.getCobra().setDireita(true);
					painel.getCobra().setCima(false);
					painel.getCobra().setBaixo(false);
	
				}
				else if(comando == KeyEvent.VK_UP && !painel.getCobra().isBaixo() && !painel.isPause()) {
					painel.getCobra().setDireita(false);
					painel.getCobra().setCima(true);
					painel.getCobra().setEsquerda(false); 
				}
				else if(comando == KeyEvent.VK_DOWN && !painel.getCobra().isCima() && !painel.isPause()) {
					painel.getCobra().setDireita(false);
					painel.getCobra().setEsquerda(false);
					painel.getCobra().setBaixo(true);
 
				}
				else if(comando ==10) { 
					if(painel.isJogando()) {
						if(!painel.isPause()) {
							painel.setPause(true);
						}
						else {
							painel.setPause(false);
						}
					}
					else {
						painel.reinicialize();
						thread = new ThreadJogo(painel);
						thread.start(); 
					}
				}
				else if(comando == 65 && !painel.isStar()) {
					painel.setKitty(true);
					
				}
				else if(comando == 83 && !painel.isKitty()) {
					painel.setStar(true);
					
				}

			}

			@Override
			public void keyReleased(KeyEvent e) {
				
			}
			
		});
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(500,500);
		setLocationRelativeTo(null);
		setResizable(false);
		setVisible(true);
		
	}
}
