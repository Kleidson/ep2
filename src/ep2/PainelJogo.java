package ep2;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image; 
public class PainelJogo extends JPanel{
	
	
	private Cobra cobra = new Cobra(); 
	private Fruta fruta = new Fruta();
	private Fruta fruta2 = new BigFruta();
	private Barreira barreiras = new Barreira(); 
	private boolean kitty = false; 
	private boolean star = false; 
	private boolean pause = false; 
	private boolean jogando = true;
	private final static int BOMB = 4; 
	private final static int BIG = 2;
	private final static int COMUM = 1;
	private final static int DECREASE = 3;
	private int contador=0;
	private int mudaFruta =0;
	private int mudaFruta2 =0; 
	private int cobraKitty =3;
	private int cobraStar =3; 
	private int mudanca =1; 
	public int quantidadecomida=0;
	private int aumentaKitty =0; 
	
	public void paint( Graphics g) {
		super.paint(g);
		setBackground(Color.black);
		ImageIcon cabe�a = new ImageIcon("imagens/cabe�a"+cobra.getIDCobra()+".png"); 
		ImageIcon corpo = new ImageIcon("imagens/corpo"+cobra.getIDCobra()+".png"); 
		ImageIcon comida = new ImageIcon("imagens/comida"+fruta.getIDFruta()+".png");
		ImageIcon comida2 = new ImageIcon("imagens/comida"+fruta2.getIDFruta()+".png");
		ImageIcon barreira = new ImageIcon("imagens/barreira.png"); 
		ImageIcon fundo= new ImageIcon("imagens/fundo.png"); 
		final Image cabe�a_ = cabe�a.getImage();
		final Image corpo_ = corpo.getImage(); 
		final Image fruta_ = comida.getImage();
		final Image fruta_2 = comida2.getImage();
		final Image barreira_ = barreira.getImage(); 
		final Image fundo_ = fundo.getImage();
		
		MudaFruta(); 
		MudaCobra(); 
		if(cobra.isViva()) {
		g.drawImage(fundo_, 0, 410, this);
		g.drawImage(fruta_, fruta.getX(), fruta.getY(), this);
		g.drawImage(fruta_2,fruta2.getX(),fruta2.getY(), this); 
		for(int j=0; j<barreiras.getQuantidade()*7; j++) {
			g.drawImage(barreira_, barreiras.getX(j), barreiras.getY(j), this);
		}
		
		for(int i=0; i<cobra.getTamanho(); i++ ) {
			if(i==0) {
				g.drawImage(cabe�a_, cobra.getX(0), cobra.getY(0), this); 
			}
			else {
				
				g.drawImage(corpo_, cobra.getX(i), cobra.getY(i), this); 
			}
		}
		DesenharBarra(g);
		if((kitty || star) && !pause) {
			contador ++; 
		}
		if(contador == 110) {
			contador = 0; 
			mudanca =1; 
			cobra = new Cobra(cobra); 
			kitty = false;
			star = false; 
		}
		}
		else {
			FinalizarJogo(g);
			jogando = false; 
		}
		if(aumentaKitty==700) {
			aumentaKitty =0; 
			cobraKitty ++; 
		}
		if(!pause) {
		mudaFruta++; 
		mudaFruta2++;
		aumentaKitty++;
		}
		g.dispose();
	}
	public void DesenharBarra(Graphics g) {
		g.setColor(Color.WHITE);
		g.drawLine(0, 410, 500, 410);
		String mensagem = "Pontua��o: "+cobra.getPontuacao(); 
		Font estilo = new Font("Consolas", Font.BOLD, 16); 
		g.setColor(Color.white);
		g.setFont(estilo);
		g.drawString(mensagem, 350, 450);
		mensagem = "("+cobraKitty+")     ("+cobraStar+")";
		estilo = new Font("Consolas", Font.BOLD, 14); 
		g.setColor(Color.white);
		g.setFont(estilo);
		g.drawString(mensagem, 30, 450);
		ImageIcon PKitty = new ImageIcon("imagens/cabe�a2.png");
		ImageIcon PStar = new ImageIcon("imagens/cabe�a3.png"); 
		final Image pKitty = PKitty.getImage();
		final Image pStar = PStar.getImage(); 
		g.drawImage(pKitty, 18, 440, this);
		g.drawImage(pStar, 82, 440, this);
		if(pause) {
			mensagem = "Pausado"; 
			g.setColor(Color.RED);
			estilo = new Font("Consolas", Font.LAYOUT_RIGHT_TO_LEFT, 20);
			g.setFont(estilo);
			g.drawString(mensagem, 200, 450);
		}
		else if(contador > 95) {
			mensagem = "Tempo acabando!"; 
			g.setColor(Color.RED);
			estilo = new Font("Consolas", Font.LAYOUT_RIGHT_TO_LEFT, 20);
			g.setFont(estilo);
			g.drawString(mensagem, 150, 450);
		}
		
	}
	public void FinalizarJogo(Graphics g) {
        String msg = "GAME OVER";
        Font pequena = new Font("Consolas", Font.BOLD, 25);
        Font fonte = new Font("Consolas", Font.BOLD, 20);
        FontMetrics metrica = this.getFontMetrics(pequena);
        g.setColor(Color.white);
        g.setFont(pequena);
        g.drawString(msg, (480 - metrica.stringWidth(msg)) / 2, 220);
        
        msg = "Sua Pontua��o: "+cobra.getPontuacao(); 
        g.setColor(Color.white);
        g.setFont(fonte);
        g.drawString(msg,150,270);
        
        msg = "PRESSIONE ENTER PARA REINICIAR";
        g.setColor(Color.white);
        fonte = new Font("Consolas", Font.ROMAN_BASELINE, 15);
        g.setFont(fonte);
        g.drawString(msg,10,450);
	}
	public Cobra getCobra() {
		return cobra;
	}

	public void setCobra(Cobra cobra) {
		this.cobra = cobra;
	}

	public Fruta getFruta() {
		return fruta;
	}

	public void setFruta(Fruta fruta) {
		this.fruta = fruta;
	}
	
	public Fruta getFruta2() {
		return fruta2;
	}
	public void setFruta2(Fruta fruta2) {
		this.fruta2 = fruta2;
	}
	public Barreira getBarreiras() {
		return barreiras;
	}
	public void setBarreiras(Barreira barreiras) {
		this.barreiras = barreiras;
	}
	public boolean isKitty() {
		return kitty;
	}
	public void setKitty(boolean kitty) {
		this.kitty = kitty;
	}
	public boolean isStar() {
		return star;
	}
	public void setStar(boolean star) {
		this.star = star;
	}
	public boolean isPause() {
		return pause;
	}
	public void setPause(boolean pause) {
		this.pause = pause;
	}
	public boolean isJogando() {
		return jogando;
	}
	public void setJogando(boolean jogando) {
		this.jogando = jogando;
	}
	public void setContador(int contador) {
		this.contador = contador;
	}
	public void setMudaFruta(int mudaFruta) {
		this.mudaFruta = mudaFruta;
	}
	public void setMudaFruta2(int mudaFruta2) {
		this.mudaFruta2 = mudaFruta2;
	}
	public void setCobraKitty(int cobraKitty) {
		this.cobraKitty = cobraKitty;
	}
	public void setCobraStar(int cobraStar) {
		this.cobraStar = cobraStar;
	}
	public void setMudanca(int mudanca) {
		this.mudanca = mudanca;
	}
	public int getCobraKitty() {
		return cobraKitty;
	}
	public int getCobraStar() {
		return cobraStar;
	}
	public void MudaFruta() {
		if(mudaFruta == 300) {
			int ParaQual = (int) (Math.random()*3)+1;
			if(ParaQual == COMUM) {
				fruta = new Fruta();
			}
			else if(ParaQual == BIG) {
				fruta = new BigFruta();
			}
			else if(ParaQual== DECREASE && cobra.getTamanho()>30) {
				fruta = new Decrease();
			}
			fruta.GerarComida(cobra, barreiras);
			mudaFruta =0; 
		}
		if(mudaFruta2==300) {
			int ParaQual = (int) (Math.random()*4)+1;
			if(ParaQual == COMUM) {
				fruta2 = new Fruta();
			}
			else if(ParaQual == BIG) {
				fruta2 = new BigFruta();
			}
			else if(ParaQual== DECREASE && cobra.getTamanho()>30) {
				fruta2 = new Decrease();
			}
			else if(ParaQual == BOMB && cobra.getPontuacao() > 30) {
				fruta2 = new Bomb(); 
			}
			fruta2.GerarComida(cobra, barreiras);
			mudaFruta2=0; 
		}
	}
	public void MudaCobra() {
		if(kitty && cobraKitty!=0 && mudanca ==1) {
			cobra = new Kitty(cobra);
			cobraKitty --; 
			mudanca=0; 
		}
		if(star && cobraStar!=0 && mudanca ==1) {
			cobra = new Star(cobra); 
			cobraStar--; 
			mudanca =0; 
		}
	}
	public void reinicialize() {
		cobra = new Cobra(); 
		fruta = new Fruta();
		fruta2 = new BigFruta();
		barreiras = new Barreira(); 
		kitty = false; 
		star = false; 
		pause = false; 
		jogando = true;
		contador=0;
		mudaFruta =0;
		mudaFruta2 =0; 
		cobraKitty =3;
		cobraStar =3; 
		mudanca =1; 
		quantidadecomida=0;
		aumentaKitty =0; 
	}
	
	
	
}
