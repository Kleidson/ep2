package ep2;

public class Interations {
	private Cobra cobra;
	private Fruta fruta;
	private Fruta fruta2;
	private PainelJogo painel; 
	 
	private Barreira barreira = new Barreira();  
	final static int KITTY = 2; 
	final static int STAR = 3; 
	final static int COMUM = 1; 
	final static int BIG = 2;
	final static int DECREASE = 3;
	final static int BOMB = 4;
	
	public Interations(PainelJogo painel, Cobra cobra, Fruta fruta, Fruta fruta2,  Barreira barreira) {
		this.painel = painel; 
		this.cobra = cobra;
		this.fruta = fruta; 
		this.fruta2 = fruta2; 
		this.barreira = barreira; 
	}
	public void Interacoes(int IDCobra, int IDFruta1, int IDFruta2) {
		ComerFruta(IDCobra,IDFruta1,1);
		ComerFruta(IDCobra,IDFruta2,2);
		Colidir(IDCobra); 
	}
	public void ComerFruta(int tipoCobra, int tipoFruta, int numeroFruta) {
		if(numeroFruta == 1) {
			if(cobra.getX(0)==fruta.getX() && cobra.getY(0)==fruta.getY()) {
				painel.setMudaFruta(0);
				switch (tipoFruta) {
				case COMUM :
						cobra.setTamanho(cobra.getTamanho()+1); 
						cobra.setPontuacao(cobra.getPontuacao()+1);
						if(tipoCobra == STAR) {
							cobra.setPontuacao(cobra.getPontuacao()+1);
						}
					break;
				case BIG:
					painel.quantidadecomida++; 
					cobra.setTamanho(cobra.getTamanho()+1);
					cobra.setPontuacao(cobra.getPontuacao()+2);
					if(tipoCobra == STAR) {
						cobra.setPontuacao(cobra.getPontuacao()+2);
					}
					if(painel.quantidadecomida > 15) {
						painel.quantidadecomida =0;
						painel.setCobraStar(painel.getCobraStar()+1);	
					}
					break;
				case DECREASE:
					cobra.setTamanho(3);
					break;
				case BOMB:
					cobra.setViva(false);
					break; 
				}
				int auxCriacao = (int)(Math.random() *40)+1;
				if(auxCriacao > 10) {
				int QualFruta = (int)(Math.random() *4)+1; 
				if(QualFruta == COMUM) {
					fruta = new Fruta(); 
				}
				else if( QualFruta == BIG) {
					fruta = new BigFruta(); 
				}
				else if(QualFruta == DECREASE) {
					if(cobra.getTamanho()>25)
						fruta = new Decrease();
					else
						fruta = new Fruta(); 
				}
				}
				else {
					if(cobra.getPontuacao()>30) {
						fruta = new Bomb(); 
					}
					else {
						fruta = new Fruta(); 
					}
				}
				fruta.GerarComida(cobra,barreira);
			}
		}
		else {
			if(cobra.getX(0)==fruta2.getX() && cobra.getY(0)==fruta2.getY()) {
				painel.setMudaFruta2(0);
				switch (tipoFruta) {
				case COMUM :
						cobra.setTamanho(cobra.getTamanho()+1); 
						cobra.setPontuacao(cobra.getPontuacao()+1);
						if(tipoCobra == STAR) {
							cobra.setPontuacao(cobra.getPontuacao()+1);
						}
					break;
				case BIG:
					painel.quantidadecomida++; 
					cobra.setTamanho(cobra.getTamanho()+1);
					cobra.setPontuacao(cobra.getPontuacao()+2);
					if(tipoCobra == STAR) {
						cobra.setPontuacao(cobra.getPontuacao()+2);
					}
					if(painel.quantidadecomida > 15) {
						painel.quantidadecomida =0;
						painel.setCobraStar(painel.getCobraStar()+1);	
					}
					break;
				case DECREASE:
					cobra.setTamanho(3);
					break; 
				}
				int QualFruta = (int)(Math.random() *3)+1; 
				if(QualFruta == COMUM) {
					fruta2 = new Fruta(); 
				}
				else if( QualFruta == BIG) {
					fruta2 = new BigFruta(); 
				}
				else {
					if(cobra.getTamanho()>25) {
						fruta2 = new Decrease(); 
					}
					else { 
						fruta2 = new Fruta(); 
					}
				}
				fruta2.GerarComida(cobra,barreira);
			}
		}
		
	}
	public void Colidir(int tipo) {
		int cond =0;
		for(int i=1; i<cobra.getTamanho(); i++) {
			if(cobra.getX(0) == cobra.getX(i) && cobra.getY(0)== cobra.getY(i)) {
			cond =1;
			}
		}
		if(tipo!= KITTY) {
			for(int i=0; i<barreira.getQuantidade()*7; i++) {
				if(cobra.getX(0) == barreira.getX(i) && cobra.getY(0) == barreira.getY(i)) {
					cond =1; 
					break;
				}
			}
		}
		if(tipo == KITTY) {
			if(cobra.getX(0)> 480 || cobra.getY(0) > 400 || cobra.getX(0)< 0 || cobra.getY(0) <0 ) {
				cond =1; 
			}
		}
		if(cond == 1) {
			cobra.setViva(false);
		}
			
	}
	public Cobra getCobra() {
		return cobra;
	}
	public void setCobra(Cobra cobra) {
		this.cobra = cobra;
	}
	public Fruta getFruta() {
		return fruta;
	}
	public void setFruta(Fruta fruta) {
		this.fruta = fruta;
	}
	public Fruta getFruta2() {
		return fruta2;
	}
	public void setFruta2(Fruta fruta2) {
		this.fruta2 = fruta2;
	}
	
}
